import math as m
import numpy as np
import sympy as sy
from sympy.utilities.autowrap import autowrap
from cyipopt import minimize_ipopt

import utils.filemanager as fm
import utils.display as disp
import utils.statesFormat as sf
import optimize.costFunction as cf
import utils.polynomialFunction as pl


def poly_optim_val(x,w,trueT,step,costFun):
	y = 0
	theTime = np.arange(0,1+step,step)
	myarg = np.append(x,0)
	for k in range(0,len(theTime)):
		myarg[-1] = theTime[k]
		for i in range(len(costFun)):
			tmpCost = costFun[i]
			tmpfun = tmpCost(myarg)
			y = y + w[(i*2)+0]*tmpfun[0]
			y = y + w[(i*2)+1]*tmpfun[1]
	return y

def poly_optim(w,nz,trueT,step,costFun,degree,Sstart,Sgoal, x0 = None):
	if x0 is None:
		x0 = np.zeros((degree+1)*2)
	fun = lambda x: poly_optim_val(x,w,trueT,step,costFun)
	cons = ({'type': 'eq',\
		'fun': lambda x:  pl.poly_constraints(x,nz,trueT,degree,Sstart,Sgoal,1),\
		'jac': lambda x: pl.poly_constraints(x,nz,trueT,degree,Sstart,Sgoal,0)})
	print("COD starting...")
	res = minimize_ipopt(fun, x0, constraints=cons)
	print("COD done")
	return res.x

def polyCOD_tau(nz,trueT,step,degree,Sstart,Sgoal,w,myMod,saveName):
	print("***** COD MODE ACTIVATE : Polynome")
	w = w / np.linalg.norm(w)
	print(w)
	funTau = autowrap(cf.function4poly(cf.symb_tau(),myMod,degree))
	functions = [funTau]
	print("Autowrap done")
	x0 = np.zeros((degree+1)*2)
	parg = poly_optim(w,nz,trueT,step,functions,degree,Sstart,Sgoal, x0)
	T=len(np.arange(0,trueT+step,step))
	realx = pl.eval_poly(parg,nz,T,trueT,step,degree)
	myfunc = autowrap(cf.symb2function(cf.symb_tau(),myMod))
	tau,dtau,ddtau = sf.tau_eval(realx, nz, T, myfunc)
	if saveName is None:
		print("COD data not saved")
	else:
		fm.csv_save(realx,w,T,nz,Sstart,Sgoal,myMod,saveName,1,degree,parg)
		print("COD data saved into "+saveName)
	restest = pl.poly_constraints(parg,nz,trueT,degree,Sstart,Sgoal,1)
	print("Constraints validity: "+str(np.sum(restest)))
	disp.display_torque(tau,T,nz,"fig/poly_tau_1.png")
	disp.display_trajectory(realx,T,nz,"fig/poly_traj_1.png")
	return parg, realx

def polyCOD_multi(nz,trueT,step,degree,Sstart,Sgoal,w,functions,myMod,saveName):
	print("***** COD MODE ACTIVATE : Polynome")
	w = w / np.linalg.norm(w)
	print(w)
	T=len(np.arange(0,trueT+step,step))
	parg = poly_optim(w,nz,trueT,step,functions,degree,Sstart,Sgoal)
	realx = pl.eval_poly(parg,nz,T,trueT,step,degree)
	myfunc = autowrap(cf.symb2function(cf.symb_tau(),myMod))
	tau,dtau,ddtau = sf.tau_eval(realx, nz, T, myfunc)
	if saveName is None:
		print("COD data not saved")
	else:
		fm.csv_save(realx,w,T,nz,Sstart,Sgoal,myMod,saveName,1,degree,parg)
		print("COD data saved into "+saveName)
	restest = pl.poly_constraints(parg,nz,trueT,degree,Sstart,Sgoal,1)
	print("Constraints validity: "+str(np.sum(restest)))
	disp.display_torque(tau,T,nz,"fig/poly_tau_1.png")
	disp.display_trajectory(realx,T,nz,"fig/poly_traj_1.png")
	return parg, realx