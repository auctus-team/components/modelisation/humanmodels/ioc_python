import warnings

import math as m
import numpy as np
import sympy as sy

import utils.polynomialFunction as pl

"""
Cost functions possible:
- angle
- velocity
- acc
- jerk
- angular power
- torque/tau
- torque change

poly : 0=simple, 1=poly, 2=polytrunc
"""

DEBUG = 0

#-------- All functions
def gradFunction_symb(mySymbFunction,poly=0,degree=None):
	if poly == 1:
		costFun = mySymbFunction
		myarg = sy.MatrixSymbol('myarg', (degree+1)*2+1, 1)
		gradFun = sy.Matrix([[costFun[0].diff(myarg[0]),costFun[0].diff(myarg[1]),costFun[0].diff(myarg[2]),costFun[0].diff(myarg[3]),costFun[0].diff(myarg[4]),costFun[0].diff(myarg[5]),costFun[0].diff(myarg[6]),costFun[0].diff(myarg[7]),costFun[0].diff(myarg[8]),costFun[0].diff(myarg[9]),costFun[0].diff(myarg[10]),costFun[0].diff(myarg[11]),costFun[0].diff(myarg[12]),costFun[0].diff(myarg[13])],\
		[costFun[1].diff(myarg[0]),costFun[1].diff(myarg[1]),costFun[1].diff(myarg[2]),costFun[1].diff(myarg[3]),costFun[1].diff(myarg[4]),costFun[1].diff(myarg[5]),costFun[1].diff(myarg[6]),costFun[1].diff(myarg[7]),costFun[1].diff(myarg[8]),costFun[1].diff(myarg[9]),costFun[1].diff(myarg[10]),costFun[1].diff(myarg[11]),costFun[1].diff(myarg[12]),costFun[1].diff(myarg[13])]])
	elif poly == 2:
		costFun = mySymbFunction
		if degree == 6:
			newarg = sy.MatrixSymbol('newarg', 2+1, 1)
			gradFun = sy.Matrix([[costFun[0].diff(newarg[0]),costFun[0].diff(newarg[1])],\
			[costFun[1].diff(newarg[0]),costFun[1].diff(newarg[1])]])
		if degree == 7:
			newarg = sy.MatrixSymbol('newarg', 4+1, 1)
			gradFun = sy.Matrix([[costFun[0].diff(newarg[0]),costFun[0].diff(newarg[1]),costFun[0].diff(newarg[2]),costFun[0].diff(newarg[3])],\
			[costFun[1].diff(newarg[0]),costFun[1].diff(newarg[1]),costFun[1].diff(newarg[2]),costFun[1].diff(newarg[3])]])
	else:
		costFun = function2cost(mySymbFunction)
		t1, t2, t3, t4, t5, t6 = sy.symbols('t1 t2 t3 t4 t5 t6', real=True)
		gradFun = sy.Matrix([[costFun[0].diff(t1),costFun[0].diff(t2),costFun[0].diff(t3),costFun[0].diff(t4),costFun[0].diff(t5),costFun[0].diff(t6)],\
			[costFun[1].diff(t1),costFun[1].diff(t2),costFun[1].diff(t3),costFun[1].diff(t4),costFun[1].diff(t5),costFun[1].diff(t6)]])
	gradFun = sy.transpose(gradFun)
	return gradFun

def gradCost(mySymbFunction,mod,poly=0,degree=None,nz=6,trueT=None,Sstart=[],Sgoal=[]):
	if poly > 0:
		myFun = function4poly(mySymbFunction,mod,degree,poly,nz,trueT,Sstart,Sgoal)
		gradFun = gradFunction_symb(myFun,poly,degree)
		return gradFun
	else:
		return symb2function(gradFunction_symb(mySymbFunction),mod)

def symb2function(mySymbFunction,mod,poly=0,degree=None,nz=6,trueT=None,Sstart=[],Sgoal=[]):
	if DEBUG:
		print('symb2function : {0}'.format(degree))
	m1, m2, l1, g = sy.symbols('m1 m2 l1 g', real=True) #L2 NEVER USED
	r1, r2, I1, I2 = sy.symbols('r1 r2 I1 I2', real=True)
	sub_fun = mySymbFunction.subs({m1: mod[0],m2: mod[1],l1: mod[2],r1: mod[4],r2: mod[5],I1: mod[6],I2: mod[7],g: 9.81})
	if poly > 0:
		t = sy.Symbol('t', real=True)
		a = sy.MatrixSymbol('a', degree+1, 1)
		b = sy.MatrixSymbol('b', degree+1, 1)
		myarg = sy.MatrixSymbol('myarg', (degree+1)*2+1, 1)
		values = {t:myarg[(degree+1)*2]}
		for i in range(degree+1):
			values.update({a[i]: myarg[i],b[i]: myarg[degree+1+i]})
		myFunction = sub_fun.subs(values)
		if poly==2:
			myFunction = pl.truncated_subs(myFunction,nz,trueT,Sstart,Sgoal,degree)
			"""a6, b6= sy.symbols('a6 b6', real=True)
			newarg = sy.MatrixSymbol('newarg', 2+1, 1)
			print(myFunction)
			print("_____________")
			myFunction = myFunction.subs({a6:newarg[0], b6:newarg[1], t:newarg[2]})
			print(myFunction)"""
	else:
		t1, t2, t3, t4, t5, t6 = sy.symbols('t1 t2 t3 t4 t5 t6', real=True)
		x = sy.MatrixSymbol('x', 6, 1)
		myFunction = sub_fun.subs([(t1, x[1-1]), (t2, x[2-1]), (t3, x[3-1]), (t4, x[4-1]), (t5, x[5-1]), (t6, x[6-1])])
		myFunction = sy.expand(myFunction, trig=True)
	return myFunction

def function2cost(mySymbFunction):
	theCost = sy.Matrix([[mySymbFunction[0]**2],[mySymbFunction[1]**2]])
	return theCost

def function4poly(mySymbFunction,mod,degree,poly=1,nz=6,trueT=None,Sstart=[],Sgoal=[]):
	if DEBUG:
		print('function4poly : {0}'.format(degree))
	return function2cost(symb2function(pl.symb_poly_function(degree, mySymbFunction),mod,poly,degree,nz,trueT,Sstart,Sgoal))

#--------- Functions lists
def symb_tau():
	a1, a2, a3 = sy.symbols('a1 a2 a3', real=True)
	b1, b2, g = sy.symbols('b1 b2 g', real=True)
	m1, m2, l1 = sy.symbols('m1 m2 l1', real=True) #L2 NEVER USED
	r1, r2, I1, I2 = sy.symbols('r1 r2 I1 I2', real=True)
	t1, t2, t3, t4, t5, t6 = sy.symbols('t1 t2 t3 t4 t5 t6', real=True)

	detM = (a3*(-a1+a3))+(a2*sy.cos(t2))**2;
	iM = (1/detM)*sy.Matrix([[-a3, a3+(a2*sy.cos(t2))],[a3+(a2*sy.cos(t2)), -a1-(2*a2*sy.cos(t2))]])
	M = sy.Matrix([[a1+(2*a2*sy.cos(t2)), a3+(a2*sy.cos(t2))],[a3+(a2*sy.cos(t2)), a3]])

	C = sy.Matrix([[-a2*t4*sy.sin(t2), -a2*(t3+t4)*sy.sin(t2)],[a2*t3*sy.sin(t2), 0]]);

	G = sy.Matrix([[(b1*g*sy.cos(t1))+(b2*g*sy.cos(t1 + t2))],[b2*g*sy.cos(t1 + t2)]]);

	th = sy.Matrix([[t1],[t2]])
	dth = sy.Matrix([[t3],[t4]])
	ddth = sy.Matrix([[t5],[t6]])

	tau = (M*ddth) + (C*dth) + G;
	tau_sy = tau.subs([(a1, (m1*r1**2)+(m2*(l1**2 + r2**2))+I1+I2), (a2, m2*l1*r2), (a3, (m2*r2**2)+I2), (b1, (l1*m2)+(r1*m1)), (b2, r2*m2)])
	return tau_sy

def symb_angulPower():
	t1, t2, t3, t4, t5, t6 = sy.symbols('t1 t2 t3 t4 t5 t6', real=True)
	tauFun = symb_tau()
	angulPowerFun = sy.Matrix([[tauFun[0]*t3],[tauFun[1]*t4]])
	return angulPowerFun

def symb_angle():
	t1, t2 = sy.symbols('t1 t2', real=True)
	angleFun = sy.Matrix([[t1],[t2]])
	return angleFun

def symb_vel():
	t3, t4 = sy.symbols('t3 t4', real=True)
	velFun = sy.Matrix([[t3],[t4]])
	return velFun

def symb_acc():
	t5, t6 = sy.symbols('t5 t6', real=True)
	velFun = sy.Matrix([[t5],[t6]])
	return velFun


