import math as m
import numpy as np
import sympy as sy
import scipy.linalg as sci
from sympy.utilities.autowrap import autowrap
from cyipopt import minimize_ipopt

import utils.filemanager as fm
import utils.display as disp
import utils.statesFormat as sf
import optimize.polyCODtrunc as ptcod
import optimize.polyCOI as pcoi
import optimize.polyCOD as cod
import optimize.costFunction as cf
import utils.polynomialFunction as pl

def gradColEval(x,trueT,step,nz,funGrad):
	gradCol = np.zeros((len(x),2*len(funGrad)))
	t = np.arange(0,trueT+step,step)
	myarg = np.append(x,0)
	for k in range(0,len(t)):
		myarg[-1] = t[k]
		for i in range(len(funGrad)):
			tmpCost = funGrad[i]
			tmpfun = tmpCost(myarg)
			gradCol[:,(i*2)+0] = gradCol[:,(i*2)+0]+tmpfun[:,0]
			gradCol[:,(i*2)+1] = gradCol[:,(i*2)+1]+tmpfun[:,1]
	return gradCol

def my_coi_matrix_trunc(x,nz,trueT,step,myMod,Gradfunctions):
	jacCost = gradColEval(x,trueT,step,nz,Gradfunctions)
	return jacCost

def polyCOItrunc_tau(mya,x,degree,nz,nc,trueT,step,myMod):
	print("***** COI MODE ACTIVATE : Truncated polynome")
	T = len(np.arange(0,trueT+step,step))
	xt = sf.vec2matrix(x,nz,T)
	Sstart = xt[0,:]
	Sgoal = xt[T-1,:]
	funGradTau = autowrap(cf.gradCost(cf.symb_tau(),myMod,2,degree,nz,trueT,Sstart,Sgoal))
	Gradfunctions = [funGradTau]
	print("autowrap done")
	print("COI starting...")
	J = my_coi_matrix_trunc(mya,nz,trueT,step,myMod,Gradfunctions)
	s = sci.svdvals(J)
	s = s / np.linalg.norm(s)
	print(s)
	print(s[-1])
	new_z,new_w = pcoi.my_resolution(J,nc)
	print("COI done")
	print(new_w)
	return new_w,s[-1]

def polyCOItrunc_multi(mya,degree,nz,nc,trueT,step,myMod,Gradfunctions):
	print("***** COI MODE ACTIVATE : Truncated polynome")
	print("COI starting...")
	J = my_coi_matrix_trunc(mya,nz,trueT,step,myMod,Gradfunctions)
	s = sci.svdvals(J)
	s = s / np.linalg.norm(s)
	print(s)
	print(s[-1])
	new_z,new_w = pcoi.my_resolution(J,nc)
	print("COI done")
	print(new_w)
	return new_w,s[-1]