# Inverse Optimal Control python

## Purpose

This project presentes the resolution of the IOC with methods called "Approximated".
Three different implementations are proposed: classical, polynomial and truncated polynomial.

## Installation

The IOC_python project is a project composed of python files.
Python 3 is required. The use of venv is recommanded.

IPOPT is also required:

```
sudo apt install gcc g++ gfortran git patch wget pkg-config liblapack-dev libmetis-dev
sudo apt-get install coinor-libipopt-dev
```


And matplotlib:
```
sudo apt install python3-matplotlib
```
To install, follow these steps:

```
git clone https://gitlab.inria.fr/jcolombe/ioc_python
cd ioc_python
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Ubuntu on Windows :
if error on matplotlib from requirements on png and freetypes:
```
sudo apt install libfreetype6-dev
sudo apt install pkg-config
```
and if it's python 3.7 or above, matplotlib 3.0.0 may not work and matplotlib 2.2.3 should be installed instead
if another error on matplotlib occured with
```
echo "backend: Agg" > ~/.config/matplotlib/matplotlibrc
```

The project may use also Pinocchio project for dynamics in the future.
Use this link to install:
https://stack-of-tasks.github.io/pinocchio/download.html


Author: Jessica Colombel, Inria
