import time

import math as m
import numpy as np
import matplotlib.pyplot as plt

import utils.filemanager as fm
import utils.display as disp
import utils.statesFormat as sf
import optimize.simpleCOD as cod

newparams = {'figure.figsize': (6.0, 6.0), 'axes.grid': True,
             'lines.markersize': 8, 'lines.linewidth': 2,
             'font.size': 14}
plt.rcParams.update(newparams)

#define function
def f(x):
    return 1/(1+x**2)#np.sin(x)

def poly(x,arg):
    y = 0
    for i in range(len(arg)):
        y = y + arg[i]*x**i
    return y

def simple_cod(nz,T,Sstart,Sgoal,w,myMod,saveName):
	start = time.time()
	x = cod.simpleCOD(nz,T,Sstart,Sgoal,w,myMod,saveName)
	end = time.time()
	hours, rem = divmod(end-start, 3600)
	minutes, seconds = divmod(rem, 60)
	print("{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds))
	return x



def omega(xdata, x):
    # compute omega(x) for the nodes in xdata
    n1 = len(xdata)
    omega_value = np.ones(len(x))
    for j in range(n1):
        omega_value = omega_value*(x-xdata[j]) # (x-x_0)(x-x_1)...(x-x_n)
    return omega_value

# chebyshev nodes
def chebyshev_nodes(a, b, n):
    # n Chebyshev noder i intervallet [a, b]
    i = np.array(range(n))
    x = np.cos((2*i+1)*m.pi/(2*(n))) # noder over intervallet [-1,1]
    return 0.5*(b-a)*x+0.5*(b+a) # noder over intervallet [a,b]

# cardinal function
def cardinal(xdata, x):
    n=len(xdata)
    l = []
    for i in range(n):
        li = np.ones(len(x))
        for j in range(n):
            if i is not j:
                li = li*(x-xdata[j])/(xdata[i]-xdata[j])
        l.append(li)
    return l

#lagrange
def lagrange(ydata, l):
    poly = 0
    for i in range(len(ydata)):
        poly = poly + ydata[i]*l[i]
    return poly

def findId(n,nodes,xtotal):
    idX = []
    for i in range(len(nodes)):
        diff = xtotal - nodes[i]
        id = np.argmin(np.abs(diff))
        idX.append(id)
    return idX

"""nz = 6;	nu = 0
T = 201
Sstart = [0,0,0,0,0,0];	Sgoal = [120*m.pi/180,40*m.pi/180,0,0,0,0]
#myMod = [1,1.5,1,1.2,0.5,0.6,0.5,0.7]
myMod = [1,1,1,1,0.5,0.5,0.5,0.5]
w = [0.6,0.4]
nc = len(w)
traj = simple_cod(nz,T,Sstart,Sgoal,w,myMod,"traj2interp")"""

x,w,myMod,T,nz,Sstart,Sgoal = fm.csv_read("res/simple_cod_T201_w2_traj2interp.csv")
x = sf.vec2matrix(x,nz,T)
trajCODi = x[:,0]
cheb = []
equi = []
allp = []
cheb2 = []
equi2 = []
allp2 = []
for j in range(100):
    noiseSample = np.random.normal(0, 1, size=len(trajCODi))
    trajCOD = trajCODi + noiseSample
    degree = 6

    a, b = 0, 1                      # interval
    x = np.linspace(a,b,T)         #x-veridene for plotting

    n = 101;

    # chebyschev nodes
    xdata = chebyshev_nodes(a, b, n+1)
    xdata = np.flip(xdata)
    idxdata = findId(n,xdata,x)
    p = np.polyfit(xdata, trajCOD[idxdata], degree)
    p = np.flip(p)
    #print(p)
    ydata = poly(xdata,p)
    ydataC = poly(x,p)
    #l = cardinal(xdata, x)
    #p = lagrange(ydata, l)

    # for equidistants points
    xdata2 = np.linspace(a, b, n+1)
    idxdata = findId(n,xdata2,x)
    p = np.polyfit(xdata2, trajCOD[idxdata], degree)
    p = np.flip(p)
    #print(p)
    ydata2 = poly(xdata2,p)
    ydata2C = poly(x,p)
    #l2 = cardinal(xdata2, x)
    #p2 = lagrange(ydata2, l2)

    # All points
    p = np.polyfit(x, trajCOD, degree)
    p = np.flip(p)
    ydata3C = poly(x,p)

    # Plot f(x) og p(x) and the interpolation points
    """fig = plt.figure()
    ax1 = fig.add_subplot(311)
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)
    ax1.plot(x, trajCODi, x, ydataC, xdata, ydata, 'o')
    ax1.legend(['f(x)','p(x)'])
    ax1.grid(True)
    ax2.plot(x, trajCODi, x, ydata2C, xdata2, ydata2, 'o')
    ax2.legend(['f(x)','p(x)'])
    ax2.grid(True)
    ax3.plot(x, omega(xdata, x))
    ax3.grid(True)"""
    #print("n = {:2d}, max|omega(x)| = {:.2e}".format(n, max(abs(omega(xdata, x)))))

    #fig.savefig("fig/test_chebyschev.png", bbox_inches='tight')

    #print("Max error Cheb is {:.2e}".format(max(abs(ydataC-trajCODi))))
    #print("Max error Equi is {:.2e}".format(max(abs(ydata2C-trajCODi))))



    cheb.append(max(abs(ydataC-trajCODi)))
    equi.append(max(abs(ydata2C-trajCODi)))
    allp.append(max(abs(ydata3C-trajCODi)))

    cheb2.append(np.mean(abs(ydataC-trajCODi)))
    equi2.append(np.mean(abs(ydata2C-trajCODi)))
    allp2.append(np.mean(abs(ydata3C-trajCODi)))

print("Max error Cheb is {:.3e}".format(np.mean(cheb)))
print("Max error Equi is {:.3e}".format(np.mean(equi)))
print("Max error All is {:.3e}".format(np.mean(allp)))

print("Mean error Cheb is {:.3e}".format(np.mean(cheb2)))
print("Mean error Equi is {:.3e}".format(np.mean(equi2)))
print("Mean error All is {:.3e}".format(np.mean(allp2)))