import sys
import time

import utils.filemanager as fm
import utils.display as disp
import utils.statesFormat as sf
import optimize.simpleCOD as cod
import optimize.simpleCOI as coi
import optimize.polyCOD as pcod
import optimize.polyCOI as pcoi
import optimize.polyCODtrunc as ptcod
import optimize.polyCOItrunc as ptcoi

import math as m
import numpy as np
import sympy as sy
import scipy.linalg as sci
import scipy.integrate as integrate

from sympy.utilities.autowrap import autowrap
from cyipopt import minimize_ipopt


def rmse(a,b):
	tmp = np.square(np.subtract(a,b))
	res = m.sqrt(np.sum(tmp)/len(a))
	return res

def mydeltapolynome(delta):
	f_delta = lambda x: delta*((-x**3)+(3.0*x**4)+(-3.0*x**5)+(x**6))
	y, err = integrate.quad(f_delta, 0, 1)
	return y

def mydeltapolynome_symb():
	delta = sy.Symbol('delta', real=True)
	t = sy.Symbol('t', real=True)
	f_delta = delta*((-t**3)+(3.0*t**4)+(-3.0*t**5)+(t**6))
	F_delta = sy.integrate(f_delta,t)
	print(F_delta)
	return y

def init_cod_trunc(nz,trueT,degree,Sstart,Sgoal,myMod):
	funTau = ptcod.symb_polytruncTau_init(degree,nz,trueT,Sstart,Sgoal,myMod)
	funTau = autowrap(funTau)
	print("Autowrap done")
	return funTau

def init_coi_trunc(nz,trueT,degree,Sstart,Sgoal,myMod):
	funGradTau_p = ptcoi.symb_gradTau_trunc(nz,trueT,Sstart,Sgoal,degree,myMod)
	funGradTau = autowrap(funGradTau_p)
	print("autowrap done")
	return funGradTau

#def errorP():
def main(myarg):
	if len(myarg)==1:
		print("***********************")
		print("At least one argument is missing:")
		print("> python3 test_cod mode [filename]")
		print("mode: int \n see options in code")
		print("***********************")
		return 0
	typeOpt=int(myarg[1])
	nz = 6;	nu = 0
	T = 101
	Sstart = [0,0,0,0,0,0];	Sgoal = [120*m.pi/180,40*m.pi/180,0,0,0,0]
	#myMod = [1,1.5,1,1.2,0.5,0.6,0.5,0.7]
	myMod = [1,1,1,1,0.5,0.5,0.5,0.5]
	w = [0.6,0.4]
	nc = len(w)
	degree = 6
	trueT = 1; step = 0.01

	if typeOpt == 1: # RMSE = f(D(alpha,beta))
		alpha = 50
		beta = -101
		T=len(np.arange(0,trueT+step,step))
		err_alphas = np.arange(0,10,0.1)
		err_betas = np.arange(0,10,0.1)
		MatrixErr = np.zeros((len(err_betas),len(err_alphas)))
		for i in range(len(err_alphas)):
			count = "states : {0} / {1} ".format(i+1,len(err_alphas))
			print(count)
			for j in range(len(err_betas)):
				a = err_alphas[i]
				b = err_betas[j]
				Xreal,Pargreal = pl.eval_poly_trunc([alpha,beta],nz,T,trueT,step,Sstart,Sgoal,degree)
				Xn,Pargn = pl.eval_poly_trunc([alpha+a,beta+b],nz,T,trueT,step,Sstart,Sgoal,degree)
				err = rmse(Xreal,Xn)
				MatrixErr[j][i] = err

		Xreal,Pargreal = pl.eval_poly_trunc([alpha,beta],nz,T,trueT,step,Sstart,Sgoal,degree)
		ydata1 = sf.vec2matrix(Xreal,nz,T)
		Xreal,Pargreal = pl.eval_poly_trunc([alpha+10,beta+10],nz,T,trueT,step,Sstart,Sgoal,degree)
		ydata2 = sf.vec2matrix(Xreal,nz,T)
		xdata = np.arange(0,trueT+step,step)

		disp.disp_matrixError(MatrixErr, err_alphas, err_betas,xdata,ydata1,ydata2,"demi_50_-101_traj")
		print(np.max(MatrixErr))
		print(np.min(MatrixErr))
	elif typeOpt == 2: # D(P)=f(D(alpha,beta))
		tmparg = sy.MatrixSymbol('tmparg', (degree+1)*2, 1)
		a0, a1, a2, a3, a4, a5, a6 = sy.symbols('a0 a1 a2 a3 a4 a5 a6', real=True)
		b0, b1, b2, b3, b4, b5, b6 = sy.symbols('b0 b1 b2 b3 b4 b5 b6', real=True)
		s1, s2, s3, s4, s5, s6 = sy.symbols('s1 s2 s3 s4 s5 s6', real=True)
		g1, g2, g3, g4, g5, g6 = sy.symbols('g1 g2 g3 g4 g5 g6', real=True)
		res = ptcod.poly_constraints(tmparg,nz,trueT,degree,sy.Matrix([s1, s2, s3, s4, s5, s6]),sy.Matrix([g1, g2, g3, g4, g5, g6]),1)
		res = sy.Matrix(res).subs({tmparg[0]:a0, tmparg[1]:a1, tmparg[2]:a2, tmparg[3]:a3, tmparg[4]:a4, tmparg[5]:a5,tmparg[6]:a6, tmparg[7]:b0, tmparg[8]:b1, tmparg[9]:b2, tmparg[10]:b3, tmparg[11]:b4, tmparg[12]:b5,tmparg[13]:b6})

		eq0 = sy.Eq(res[0,0])
		eq1 = sy.Eq(res[1,0])
		eq2 = sy.Eq(res[2,0])
		eq3 = sy.Eq(res[3,0])
		eq4 = sy.Eq(res[4,0])
		eq5 = sy.Eq(res[5,0])
		eq6 = sy.Eq(res[6,0])
		eq7 = sy.Eq(res[7,0])
		eq8 = sy.Eq(res[8,0])
		eq9 = sy.Eq(res[9,0])
		eq10 = sy.Eq(res[10,0])
		eq11 = sy.Eq(res[11,0])
		res = sy.solve([eq0,eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8,eq9,eq10,eq11],[a0, a1, a2, a3, a4, a5, b0, b1, b2, b3, b4, b5])
		newres = sy.Matrix([res[a0],res[a1],res[a2],res[a3],res[a4],res[a5],a6])

		da0, da1, da2, da3, da4, da5, da6 = sy.symbols('da0 da1 da2 da3 da4 da5 da6', real=True)
		db0, db1, db2, db3, db4, db5, db6 = sy.symbols('db0 db1 db2 db3 db4 db5 db6', real=True)
		ds1, ds2, ds3, ds4, ds5, ds6 = sy.symbols('ds1 ds2 ds3 ds4 ds5 ds6', real=True)
		dg1, dg2, dg3, dg4, dg5, dg6 = sy.symbols('dg1 dg2 dg3 dg4 dg5 dg6', real=True)
		resd = ptcod.poly_constraints(tmparg,nz,trueT,degree,sy.Matrix([ds1, ds2, ds3, ds4, ds5, ds6]),sy.Matrix([dg1, dg2, dg3, dg4, dg5, dg6]),1)
		resd = sy.Matrix(resd).subs({tmparg[0]:da0, tmparg[1]:da1, tmparg[2]:da2, tmparg[3]:da3, tmparg[4]:da4, tmparg[5]:da5,tmparg[6]:da6, tmparg[7]:db0, tmparg[8]:db1, tmparg[9]:db2, tmparg[10]:db3, tmparg[11]:db4, tmparg[12]:db5,tmparg[13]:db6})
		eq0 = sy.Eq(resd[0,0])
		eq1 = sy.Eq(resd[1,0])
		eq2 = sy.Eq(resd[2,0])
		eq3 = sy.Eq(resd[3,0])
		eq4 = sy.Eq(resd[4,0])
		eq5 = sy.Eq(resd[5,0])
		eq6 = sy.Eq(resd[6,0])
		eq7 = sy.Eq(resd[7,0])
		eq8 = sy.Eq(resd[8,0])
		eq9 = sy.Eq(resd[9,0])
		eq10 = sy.Eq(resd[10,0])
		eq11 = sy.Eq(resd[11,0])
		resd = sy.solve([eq0,eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8,eq9,eq10,eq11],[da0, da1, da2, da3, da4, da5, db0, db1, db2, db3, db4, db5])
		newresd = sy.Matrix([resd[da0],resd[da1],resd[da2],resd[da3],resd[da4],resd[da5],da6])
		print('____________________________')
		deltaRes = newres-newresd
		print(deltaRes)
		deltaa6, deltas1, deltas2, deltas3, deltas4, deltas5, deltas6 = sy.symbols('deltaa6 deltas1 deltas2 deltas3 deltas4 deltas5 deltas6', real=True)
	elif typeOpt == 3: # D(alpha,beta) = f(D(w))
		myarg = sy.MatrixSymbol('myarg', (degree+1)*2+1, 1)
		tau = pcod.symb_polyTau_init(degree, myMod)
		costTau = sy.Matrix([tau[0]**2,tau[1]**2])

		t = sy.Symbol('t')
		a0, a1, a2, a3, a4, a5, a6 = sy.symbols('a0 a1 a2 a3 a4 a5 a6', real=True)
		b0, b1, b2, b3, b4, b5, b6 = sy.symbols('b0 b1 b2 b3 b4 b5 b6', real=True)
		costTau = costTau.subs({myarg[0]:a0, myarg[1]:a1, myarg[2]:a2, myarg[3]:a3, myarg[4]:a4, myarg[5]:a5,myarg[6]:a6, myarg[7]:b0, myarg[8]:b1, myarg[9]:b2, myarg[10]:b3, myarg[11]:b4, myarg[12]:b5,myarg[13]:b6,myarg[14]:t})
		values = ptcod.truncated_poly(nz,trueT,Sstart,Sgoal,degree)
		costTauTrunc = costTau.subs(values)

		gradTau = sy.Matrix([[costTauTrunc[0].diff(a6),costTauTrunc[0].diff(b6)],\
			[costTauTrunc[1].diff(a6),costTauTrunc[1].diff(b6)]])
		J = sy.transpose(gradTau)
		print(J)
		print("________________________")
		print("________________________")
		H = sy.Matrix([[J[0,0].diff(a6),J[0,1].diff(a6)],\
			[J[1,0].diff(b6),J[1,0].diff(b6)]])
		print(H)



if __name__ == '__main__':
	main(sys.argv)