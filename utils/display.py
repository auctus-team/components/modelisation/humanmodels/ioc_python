import glob
from pylab import *
import palettable.colorbrewer.qualitative as pt
import numpy as np

import matplotlib.pyplot as plt

linestyle_str = [
     ('solid', 'solid'),      # Same as (0, ()) or '-'
     ('dotted', 'dotted'),    # Same as (0, (1, 1)) or ':'
     ('dashed', 'dashed'),    # Same as '--'
     ('dashdot', 'dashdot')]  # Same as '-.'

def simple_figure(xdata,ydata,filename):
	params = {
	'axes.labelsize': 8,
	'font.size': 8,
	'legend.fontsize': 8,
	'xtick.labelsize': 10,
	'ytick.labelsize': 10,
	'text.usetex': False,
	'figure.figsize': [12, 6]
	}
	colors = pt.Set2_4.mpl_colors
	filename="fig/test_simplefig_{0}.png".format(filename)

	fig, ax = subplots()
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.spines['left'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()
	ax.tick_params(axis='x', direction='out')
	ax.tick_params(axis='y', length=0)
	ax.grid(axis='x', color="0.9", linestyle='-', linewidth=1)
	si = np.shape(ydata)
	if len(si) == 1:
		leny = 1
	else:
		leny = si[1]

	for i in range(leny):
		if leny==1:
			ax.plot(xdata, ydata, linewidth=2, color=colors[i])
		else:
			ax.plot(xdata, ydata[:,i], linewidth=2, color=colors[i])

	fig.savefig(filename, bbox_inches='tight')
	print('Save fig')

def simple_plot(ax,xdata,ydata,legends,colors,mylinestyle='-'):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.spines['left'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()
	ax.tick_params(axis='x', direction='out')
	ax.tick_params(axis='y', length=0)
	ax.grid(axis='x', color="0.9", linestyle='-', linewidth=1)
	ax.plot(xdata, ydata[:,0], linestyle=mylinestyle, linewidth=2, color=colors[0])
	ax.plot(xdata, ydata[:,1], linestyle=mylinestyle, linewidth=2, color=colors[1])
	legend = ax.legend(legends, loc=4)
	frame = legend.get_frame()
	frame.set_facecolor('1.0')
	frame.set_edgecolor('1.0')

def display_torque(tau,T,nz,filename):
	params = {
	'axes.labelsize': 8,
	'font.size': 8,
	'legend.fontsize': 8,
	'xtick.labelsize': 10,
	'ytick.labelsize': 10,
	'text.usetex': False,
	'figure.figsize': [6, 3]
	}
	colors = pt.Set2_3.mpl_colors
	rcParams.update(params)

	ydata = tau
	si = np.shape(ydata)
	if len(si) == 1:
		ydata=tau.reshape((T,2))
		leny = 1
	elif len(si) == 2:
		ydata=tau
		leny = 1
	else:
		leny = si[2]
	xdata=range(0,T)

	fig = figure()
	ax = fig.add_subplot(111)
	for i in range(leny):
		if leny==1:
			simple_plot(ax,xdata,ydata,["Tau 1","Tau 2"],colors)
		else:
			simple_plot(ax,xdata,ydata[:,:,i],["Tau 1","Tau 2"],colors,linestyle_str[i][1])

	fig.savefig(filename, bbox_inches='tight')
	print('Save fig')

def display_trajectory(ydata,T,nz,filename):
	params = {
	'axes.labelsize': 8,
	'font.size': 8,
	'legend.fontsize': 8,
	'xtick.labelsize': 10,
	'ytick.labelsize': 10,
	'text.usetex': False,
	'figure.figsize': [12, 6]
	}
	colors = pt.Set2_3.mpl_colors
	rcParams.update(params)

	xdata=range(0,T)

	fig = figure()
	ax1 = fig.add_subplot(311)
	ax2 = fig.add_subplot(312)
	ax3 = fig.add_subplot(313)
	si = np.shape(ydata)
	if len(si) == 1:
		ydata=ydata.reshape((T,nz))
		leny = 1
	else:
		leny = si[1]
		ydata=ydata.reshape((T,nz,leny))


	for i in range(leny):
		if leny==1:
			simple_plot(ax1,xdata,ydata[:,0:2],["Theta 1","Theta 2"],colors)
			simple_plot(ax2,xdata,ydata[:,2:4],["dTheta 1","dTheta 2"],colors)
			simple_plot(ax3,xdata,ydata[:,4:6],["ddTheta 1","ddTheta 2"],colors)
		else:
			simple_plot(ax1,xdata,ydata[:,0:2,i],["Theta 1","Theta 2"],colors,linestyle_str[i][1])
			simple_plot(ax2,xdata,ydata[:,2:4,i],["dTheta 1","dTheta 2"],colors,linestyle_str[i][1])
			simple_plot(ax3,xdata,ydata[:,4:6,i],["ddTheta 1","ddTheta 2"],colors,linestyle_str[i][1])


	fig.savefig(filename, bbox_inches='tight')
	print('Save fig')

def mymatrix_plot(ax,xdata,ydata,legends,colors):
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)
	ax.spines['left'].set_visible(False)
	ax.get_xaxis().tick_bottom()
	ax.get_yaxis().tick_left()
	ax.tick_params(axis='x', direction='out')
	ax.tick_params(axis='y', length=0)
	ax.grid(axis='x', color="0.9", linestyle='-', linewidth=1)
	ax.plot(xdata, ydata[:,0], linewidth=2, color=colors[0])
	ax.plot(xdata, ydata[:,1], linewidth=2, color=colors[1])
	legend = ax.legend(legends, loc=4)
	frame = legend.get_frame()
	frame.set_facecolor('1.0')
	frame.set_edgecolor('1.0')

def find_nearest_w(Mw,valueW):
	min_index = 0
	min_normw = 2
	for i in range(len(valueW)):
		err = np.linalg.norm(valueW[i,:] - Mw)
		if err < min_normw:
			min_normw = err
			min_index = i
	print(min_normw)
	print(valueW[min_index,:])
	print(Mw)
	print(valueW)
	return min_index

def display_matrix(M,Mp,Mw,alp,bet,ydata,xdata,valueW,filename):
	params = {
	'axes.labelsize': 8,
	'font.size': 8,
	'legend.fontsize': 8,
	'xtick.labelsize': 10,
	'ytick.labelsize': 10,
	'text.usetex': False,
	'figure.figsize': [24, 24]
	}
	rcParams.update(params)
	filename="fig/test_matrix_{0}.png".format(filename)

	fig, ax = subplots()

	if len(M) > 0:
		extent = [np.min(alp),np.max(alp),np.min(bet),np.max(bet)]
		M = np.flip(M,0)
		ax.matshow(M, extent=extent, cmap=cm.YlGnBu)#
	#plt.colorbar()
	#ax.hold(True)
	#ax.set_xlim(np.min(alp), np.max(alp))
	#ax.set_ylim(np.min(bet), np.max(bet))
	#ax.set_xticks(alp)
	#ax.set_yticks(bet)
	for i in range(len(xdata)):
		ax.scatter(xdata[i], ydata[i], marker='o', color=(valueW[i][0], valueW[i][1], 0.5), label='point')
	if len(Mp) > 0:
		si = np.shape(Mp)
		print(si)
		for t in range(si[2]):
			for k in range(si[0]):
				if Mw[k][0][t] < 0 or Mw[k][1][t] < 0:
					if t==0:
						ax.scatter(Mp[k][0][t], Mp[k][1][t], marker='D', color=(0.5, 0.5, 0.5), label='point')
					else:
						ax.scatter(Mp[k][0][t], Mp[k][1][t], marker='D', color=(0.1, 0.1, 0.1), label='point')
				else:
					ax.scatter(Mp[k][0][t], Mp[k][1][t], marker='x', color=(Mw[0][0][t], Mw[0][1][t], 0.5), label='point')
					#ind = find_nearest_w(Mw[k,:],valueW)
					ax.plot([Mp[k][2][t],Mp[k][0][t]],[Mp[k][3][t],Mp[k][1][t]],color=(0.5,0.5,0.5))
	fig.savefig(filename, bbox_inches='tight')

def disp_matrixError(M,alp,bet,xdata,ydata1,ydata2,filename):
	params = {
	'axes.labelsize': 8,
	'font.size': 8,
	'legend.fontsize': 8,
	'xtick.labelsize': 10,
	'ytick.labelsize': 10,
	'text.usetex': False,
	'figure.figsize': [24, 24]
	}
	rcParams.update(params)
	filename="fig/error_matrix_{0}.png".format(filename)
	colors = pt.Set2_3.mpl_colors
	fig = figure()
	ax1 = fig.add_subplot(411)
	ax2 = fig.add_subplot(423)
	ax3 = fig.add_subplot(424)
	ax4 = fig.add_subplot(425)
	ax5 = fig.add_subplot(426)
	ax6 = fig.add_subplot(427)
	ax7 = fig.add_subplot(428)

	extent = [np.min(alp),np.max(alp),np.min(bet),np.max(bet)]
	M = np.flip(M,0)
	ax1.matshow(M, extent=extent, cmap=cm.hot)

	simple_plot(ax2,xdata,np.transpose(np.array([ydata1[:,0],ydata2[:,0]])),["Theta 1","Theta 1 - max"],colors)
	simple_plot(ax3,xdata,np.transpose(np.array([ydata1[:,1],ydata2[:,1]])),["Theta 2","Theta 2 - max"],colors)
	simple_plot(ax4,xdata,np.transpose(np.array([ydata1[:,2],ydata2[:,2]])),["dTheta 1","dTheta 1 - max"],colors)
	simple_plot(ax5,xdata,np.transpose(np.array([ydata1[:,3],ydata2[:,3]])),["dTheta 2","dTheta 2 - max"],colors)
	simple_plot(ax6,xdata,np.transpose(np.array([ydata1[:,4],ydata2[:,4]])),["ddTheta 1","ddTheta 1 - max"],colors)
	simple_plot(ax7,xdata,np.transpose(np.array([ydata1[:,5],ydata2[:,5]])),["ddTheta 2","ddTheta 2 - max"],colors)


	fig.savefig(filename, bbox_inches='tight')
