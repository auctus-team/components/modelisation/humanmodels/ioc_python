import math as m
import numpy as np
import sympy as sy

def vec2matrix(x,nz,T):
	return x.reshape((T,nz))

def matrix2vec(x,nz,T):
	return x.reshape(nz*T)

def tau_eval(x, nz, T, funTau):
	tau = np.zeros((T,2))
	dtau = np.zeros((T,2))
	ddtau = np.zeros((T,2))

	dt = 1/T
	DeltaT = dt

	for k in range(0,T):
		it = k*nz - 1
		tmpTau = funTau(np.array([x[it+1],x[it+2],x[it+3],x[it+4],x[it+5],x[it+6]]))
		tau[k][:] = [tmpTau[0],tmpTau[1]]

	for k in range(0,T-1):
		for i in range(0,1):
			dtau[k][i] = (tau[k+1][i]-tau[k][i])/DeltaT
	dtau[T-1][:] = [0,0]

	for k in range(0,T-1):
		for i in range(0,1):
			ddtau[k][i] = (dtau[k+1][i]-dtau[k][i])/DeltaT
	ddtau[T-1][:] = [0,0]

	return tau,dtau,ddtau

def traj2initgoal(x,nz,T):
	X = vec2matrix(x,nz,T)
	Sstart = X[0,:]
	Sgoal = X[-1,:]
	return Sstart,Sgoal