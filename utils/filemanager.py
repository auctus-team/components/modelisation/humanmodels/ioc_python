import csv
import numpy as np

def csv_save(x,w,T,nz,Sstart,Sgoal,Mod,someName,poly,degree,parg):
	sh = np.shape(w)
	if poly==1:
		filename="res/poly_cod_T{0}_w{1}_{2}.csv".format(T, sh[0],someName)
	else:
		filename="res/simple_cod_T{0}_w{1}_{2}.csv".format(T, sh[0],someName)

	with open(filename, 'w', newline='') as csvfile:
		spamwriter = csv.writer(csvfile, delimiter=',',\
			quotechar='|', quoting=csv.QUOTE_MINIMAL)
		spamwriter.writerow(['Type',poly])
		spamwriter.writerow(['T',T])
		spamwriter.writerow(['nz',nz])
		header = 'Mod'
		new = np.hstack([header,Mod])
		spamwriter.writerow([r for r in new])
		header = 'w'
		new = np.hstack([header,w])
		spamwriter.writerow([r for r in new])
		header = 'Sstart'
		new = np.hstack([header,Sstart])
		spamwriter.writerow([r for r in new])
		header = 'Sgoal'
		new = np.hstack([header,Sgoal])
		spamwriter.writerow([r for r in new])
		if poly==1:
			spamwriter.writerow(['degree',degree])
			header = 'Polynome'
			new = np.hstack([header,parg])
			spamwriter.writerow([r for r in new])
		header = 'X'
		new = np.hstack([header,x])
		spamwriter.writerow([r for r in new])

def csv_read(filename):
	with open(filename, 'r', newline='') as csvfile:
		reader = csv.reader(csvfile)
		for row in reader:
			if row[0]=='Type':
				polyType = int(row[1])
			if row[0]=='T':
				T = int(row[1])
			if row[0]=='nz':
				nz = int(row[1])
			if row[0]=='Mod':
				myMod = row[1:]
				myMod = np.array(myMod).astype(np.float)
			if row[0]=='w':
				w = row[1:]
				w = np.array(w).astype(np.float)
			if row[0]=='Sstart':
				Sstart = row[1:]
				Sstart = np.array(Sstart).astype(np.float)
			if row[0]=='Sgoal':
				Sgoal = row[1:]
				Sgoal = np.array(Sgoal).astype(np.float)
			if row[0]=='X':
				x = row[1:]
				x = np.array(x).astype(np.float)
			if row[0]=='degree':
				degree = int(row[1])
			if row[0]=='Polynome':
				parg = row[1:]
				parg = np.array(parg).astype(np.float)
	if polyType:
		return x,parg,degree,w,myMod,T,nz,Sstart,Sgoal
	else:
		return x,w,myMod,T,nz,Sstart,Sgoal

def save_matrices(ydata,xdata,valuesW,someName):
	filename="res/matrices_{0}.csv".format(someName)
	with open(filename, 'w', newline='') as csvfile:
		spamwriter = csv.writer(csvfile, delimiter=',',\
			quotechar='|', quoting=csv.QUOTE_MINIMAL)
		header = 'X'
		new = np.hstack([header,xdata])
		spamwriter.writerow([r for r in new])
		header = 'Y'
		new = np.hstack([header,ydata])
		spamwriter.writerow([r for r in new])
		header = 'W1'
		new = np.hstack([header,valuesW[:,0]])
		spamwriter.writerow([r for r in new])
		header = 'W2'
		new = np.hstack([header,valuesW[:,1]])
		spamwriter.writerow([r for r in new])

def read_matrices(filename):
	with open(filename, 'r', newline='') as csvfile:
		reader = csv.reader(csvfile)
		for row in reader:
			if row[0]=='X':
				xdata = row[1:]
				xdata = np.array(xdata).astype(np.float)
			if row[0]=='Y':
				ydata = row[1:]
				ydata = np.array(ydata).astype(np.float)
			if row[0]=='W1':
				w1 = row[1:]
				w1 = np.array(w1).astype(np.float)
			if row[0]=='W2':
				w2 = row[1:]
				w2 = np.array(w2).astype(np.float)
		valuesW = np.zeros((len(w1),2))
		valuesW[:,0] = w1
		valuesW[:,1] = w2
		return xdata,ydata,valuesW